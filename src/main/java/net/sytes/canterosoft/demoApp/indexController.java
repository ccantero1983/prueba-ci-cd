package net.sytes.canterosoft.demoApp;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class indexController {

    @GetMapping({"/","home","index"})
    public String index(Model model){
        model.addAttribute("titulo","Hola Mundo");
        return "index";
    }
}
